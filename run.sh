if [[ -z ${1} ]]; then
	echo "Provide the name of the run"
	exit 1
fi
RUN_NAME=${1}
COUNTDOWN=${2:-5}

echo "Starting Speedrun '${1}' in:"
while [ $COUNTDOWN -gt 0 ]; do
	echo "... ${COUNTDOWN}"
	let COUNTDOWN-=1
	sleep 1
done

sudo python3 pySplitter.py $1
