from os.path import dirname, abspath
import sys

if __name__ == '__main__':
    sys.path.append(dirname(dirname(abspath(__file__))))
    from pySplitter import main
    sys.exit(main())
