import keyboard
from pySplitter.config import cfg
from pySplitter.timers import SimpleTimer, ComparisonTimer

def main():
    cfg.configure()
    if cfg['compare'] == 'practice':
        s = SimpleTimer()
    else:
        s = ComparisonTimer()
    
    split_key = cfg['split_key']
    s.start()
    try:
        for _ in s.splits:
            keyboard.wait(split_key)
            s.split()
    except (KeyboardInterrupt, EOFError):
        s.done = True  # break out of the timer's main loop...
        s.cancel = True  # ...and set a flag not to run finish()
    finally:
        s.join()

    return 0
# def main()
